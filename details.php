 <!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php include'layouts/header.php'; 
$msg = '';
$product_id = $_GET['id'];?>

		
<!-- //navigation -->
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Details</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
<div class="newproducts-w3agile">
		<div class="container">
			<h4></h4>
				<div class="agile_top_brands_grids">
					  <?php 
					  $stmtproductSelect = $conn->prepare("SELECT * FROM tbl_product WHERE sub_cat_id=:product_id");
						$stmtproductSelect->bindParam(':product_id',$product_id);
						$stmtproductSelect->execute();

						$stmtproductSelect->setFetchMode(PDO::FETCH_ASSOC);
                                        foreach ($stmtproductSelect->fetchAll() as $key => $product):
                                        	$product_id=$product['product_id']

						?>
				
					<div class="col-md-3 top_brand_left-1" style="margin-bottom: 50px">
						<div class="hover14 column">
							<div class="agile_top_brand_left_grid">
								<div class="agile_top_brand_left_grid_pos">
									
								</div>
								<div class="agile_top_brand_left_grid1">
									<figure>
										<div class="snipcart-item block">
											<div class="snipcart-thumb">
												<a href="<?php echo 'description.php?product_id='.$product_id; ?>"><img height="180px" alt=" " src="Admin/<?php echo $product['product_image']; ?>"></a>		
												<p><b><?php echo $product['product_title']; ?></b></p>
											
													<h4>Rs. <?php echo $product['Price']; ?></span></h4>
											</div>
											<div class="snipcart-details top_brand_home_details">
												<form action="#" method="post">
													<fieldset>
														<input type="hidden" name="cmd" value="_cart">
														<input type="hidden" name="add" value="1">
														<input type="hidden" name="business" value=" ">
														<input type="hidden" name="item_name" value="Fortune Sunflower Oil">
														<input type="hidden" name="amount" value="35.99">
														<input type="hidden" name="discount_amount" value="1.00">
														<input type="hidden" name="currency_code" value="USD">
														<input type="hidden" name="return" value=" ">
														<input type="hidden" name="cancel_return" value=" ">
														<!--
														<input type="submit" name="submit" value="Add to cart" class="button"> 
														-->
													</fieldset>
												</form>
											</div>
										</div>
									</figure>
								</div>
							</div>
						</div>
					</div>

					<?php endforeach; ?>
				
						<div class="clearfix"> </div>
				</div>
		</div>
	</div>
<!-- //new -->
<!---728x90--->
<!-- //footer -->
<?php include'layouts/footer.php'; ?>