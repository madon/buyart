<?php include'layouts/header.php'; ?>
<?php

session_start(); 
if(isset($_SESSION['user_role']))
{
	
	header('location:index.php');
}

$msg = '';
if(isset($_POST['loginbtn']))
{	
	
	$email = $_POST['email'];
	$password =md5($_POST['password']);
	$stmtLogin = $conn->prepare("SELECT * FROM tbl_user_login WHERE user_email=:user_email AND user_password=:user_password AND user_status='1'");
	$stmtLogin->bindParam(':user_email',$email);
	$stmtLogin->bindParam(':user_password',$password);
	$stmtLogin->execute();
	if($stmtLogin->rowCount()>0)
	{
		$userInfo = $stmtLogin->fetch();
		$_SESSION['user_id'] = $userInfo['user_id'];
		$_SESSION['user_role'] = $userInfo['user_role'];
		$_SESSION['user_name'] = $userInfo['user_fname']. " " .  $userInfo['user_lname'];
		header('location:index.php');
	}
	else
	{
		$msg = "Invalid Username Or Password";
	}

}
 



?>


<!---728x90--->
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Login Page</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
<!---728x90--->
<!-- login -->
	<div class="login">
		<div class="container">
			<h2>Login Form</h2>
			<h6><?php echo $msg; ?></h6>
		
			<div class="login-form-grids animated wow slideInUp" data-wow-delay=".5s">
				<form method="post">
					<input type="email" name="email" placeholder="Email Address" required=" " >
					<input type="password" name="password" placeholder="Password" required=" " >
					<div class="forgot">
						<a href="#">Forgot Password?</a>
					</div>
					<input type="submit" name="loginbtn" value="Login">
				</form>
			</div>
			<h4>For New People</h4>
			<p><a href="registered.php">Register Here</a> (Or) go back to <a href="index.php">Home<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a></p>
		</div>
	</div>
<!-- //login -->
<!---728x90--->

<?php include'layouts/footer.php'?>