<?php include"layouts/header.php";?>
		
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Buy Now</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
<!---728x90--->
<!-- login -->
	<div class="login">
		<div class="container">
			<h2>Confirmation Form</h2>
		
		
			<div class="login-form-grids animated wow slideInUp" data-wow-delay=".5s">
				<form method="post" id="buyform">
					<input type="text" name="text" id="fname" placeholder="Full Name"  >
					<input type="email" name="email" id="email" placeholder="Email Address"  >
					<input type="text" name="phone_no" id="phone_no" placeholder="Phone Number"  >
					<input type="text" name="location" id="location" placeholder="Location"  >
					
					
					
					<div class="forgot">
					</div>
					<input type="submit" id="buynow" name="buybtn" value="Buy">
				</form>
			</div>
			
		</div>
	</div>
	<script type="text/javascript">
$('#buyform').submit(function() {
  var filter = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  var number= /[0-9 -()+]+$/;
  var alpha= /^[a-zA-Z0-9!-”$%&’()*\+,\/;\[\\\]\/\s^_.`{|}~]+$/;
  var phone_no=/^(?:\+\d{2})?\d{10}(?:,(?:\+\d{2})?\d{10})*$/;

  
    var fname =$('#fname').val();
    var location =$('#location').val();
   
    var email =$('#email').val();
    
    var contact =$('#contact').val();

    

   if(!alpha.test(fname))
  {
    $("#fname").css({"border": "1px solid red"});
 
   $('#fname').focus();
   setTimeout(function() {
       $('#fname').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }

  

  if(!alpha.test(email))
  {
    $("#email").css({"border": "1px solid red"});
 
   $('#email').focus();
   setTimeout(function() {
       $('#email').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }

  
  

if(!number.test(phone_no))
  {
    $("#phone_no").css({"border": "1px solid red"});
 
   $('#phone_no').focus();
   setTimeout(function() {
       $('#phone_no').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }
  if(!alpha.test(location))
  {
    $("#location").css({"border": "1px solid red"});
 
   $('#location').focus();
   setTimeout(function() {
       $('#location').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }

  else
  {
    $('#buyform').submit();
  }
});

</script>
<!-- //login -->
<!---728x90--->

<?php include'layouts/footer.php'?>