<?php include 'layouts/Header.php';
$msg = '';
$slider_id = $_GET['id'];
if(isset($_POST['savebtn']))
{

$slider_title = $_POST['slider_title'];
$slider_desc = $_POST['slider_desc'];
$slider_status = $_POST['slider_status'];
$slider_position = $_POST['slider_position'];


	/*Upload Photo*/
	if(!empty($_FILES['slider_image']['name']))
	{

	$name=$_FILES['slider_image']['name'];
	$src=$_FILES['slider_image']['tmp_name'];
	$newname=time();
	$temp=explode(".",$name);
	$newpath=$newname.".".$temp[1];
	$name=$newpath;
	$target_dir="slider_image/";
	$folder="slider_image";
	if(!is_dir($folder))
	{
		mkdir($folder,0755);
	}
	move_uploaded_file($src,$target_dir.$name);
	$path = $target_dir.$name;	
	

	$stmtSlider = $conn->prepare("UPDATE  tbl_slider  SET 
	slider_title=:slider_title, slider_desc=:slider_desc,  slider_status=:slider_status, slider_position=:slider_position, slider_update=CURRENT_TIMESTAMP, slider_image=:slider_image WHERE slider_id=:slider_id");
	$stmtSlider->bindParam('slider_title',$slider_title);
	$stmtSlider->bindParam('slider_desc',$slider_desc);
	$stmtSlider->bindParam('slider_position',$slider_position);
	$stmtSlider->bindParam('slider_status',$slider_status);
	$stmtSlider->bindParam('slider_image',$path);
	$stmtSlider->bindParam('slider_id',$slider_id);
	if($stmtSlider->execute())
	{
	$msg = '<div class="alert alert-info no-border">
			<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
				<span class="text-semibold">Congrats!</span>
				Slider Updated Successfully !!!</div>';
	}
	}

	else
	{

	$stmtSlider = $conn->prepare("UPDATE  tbl_slider  SET 
	slider_title=:slider_title, slider_desc=:slider_desc, slider_position=:slider_position, slider_status=:slider_status, slider_update=CURRENT_TIMESTAMP WHERE slider_id=:slider_id");
	$stmtSlider->bindParam('slider_title',$slider_title);
	$stmtSlider->bindParam('slider_desc',$slider_desc);
	$stmtSlider->bindParam('slider_position',$slider_position);
	$stmtSlider->bindParam('slider_status',$slider_status);
	$stmtSlider->bindParam('slider_id',$slider_id);
	if($stmtSlider->execute())
	{
	$msg = '<div class="alert alert-info no-border">
			<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
				<span class="text-semibold">Congrats!</span>
				Slider Updated Successfully !!!</div>';
	}

	}
	



}


$stmtSliderSelect = $conn->prepare("SELECT * FROM tbl_slider WHERE slider_id=:slider_id");
$stmtSliderSelect->bindParam('slider_id',$slider_id);
$stmtSliderSelect->execute();
$info = $stmtSliderSelect->fetch();
//print_r($info);
 ?>

	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">Victoria Baker</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<?php  include 'layouts/sidebar.php'; ?>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Forms</span> - Slider Add Form</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					
					<?php echo $msg; ?>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Form horizontal -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Add Slider</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							

							<form class="form-horizontal" action="" method="POST" name="sliderform" id="sliderform" enctype="multipart/form-data">
								<fieldset class="content-group">
									<legend class="text-bold">Information</legend>

									<div class="form-group">
										<label class="control-label col-lg-2">Slider Title</label>
										<div class="col-lg-10">
											<input type="text" name="slider_title" id="slider_title" class="form-control" placeholder="Slider Title" value="<?php echo $info['slider_title']; ?>">
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Slider Description</label>
										<div class="col-lg-10">
											<textarea class="form-control" name="slider_desc" id="slider_desc" placeholder="Description..."><?php echo $info['slider_desc']; ?></textarea>
										</div>
									 </div>


									 <div class="form-group">
										<label class="control-label col-lg-2">Previous Image</label>
										<div class="col-lg-10">
										<img src="<?php echo $info['slider_image']; ?>" height="250px" width="500px">
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Slider Image</label>
										<div class="col-lg-10">
											<input type="file" name="slider_image" id="slider_image" class="form-control" >
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Status</label>
										<div class="col-lg-10">
										 <input type="radio" name="slider_status" <?php if($info['slider_status']==1) echo 'checked'; ?> value="1"> Active

										 <input type="radio" name="slider_status" <?php if($info['slider_status']==0) echo 'checked'; ?> value="0"> Inactive
										</div>
									</div>

									

			                        <div class="form-group">
			                        	<label class="control-label col-lg-2">Slider Position</label>
			                        	<div class="col-lg-10">
				                            <select  name="slider_position" class="form-control" id="slider_position">
				                            	<option value="0">--Select One--</option>
				                                <option value="1"  <?php if($info['slider_position']==1) echo 'selected="selected"'; ?> > 1</option>
				                                <option value="2"  <?php if($info['slider_position']==2) echo 'selected="selected"'; ?>> 2</option>
				                                <option value="3" <?php if($info['slider_position']==3) echo 'selected="selected"'; ?>> 3</option>
				                                <option value="4" <?php if($info['slider_position']==4) echo 'selected="selected"'; ?>> 4</option>
				                                <option value="5" <?php if($info['slider_position']==5) echo 'selected="selected"'; ?>> 5</option>
				                               
				                            </select>
			                            </div>
			                        </div>

									
								</fieldset>

								

								<div class="text-right">
									<button type="submit" name="savebtn" class="btn btn-primary"> Update <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</form>
						</div>
					</div>
					<!-- /form horizontal -->

					
					<?php  include 'layouts/footer.php'; ?>
<script type="text/javascript">
$('#sliderform').submit(function() {
  var filter = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  var number= /[0-9 -()+]+$/;
  var alpha= /^[a-zA-Z0-9!-”$%&’()*\+,\/;\[\\\]\/\s^_.`{|}~]+$/;
  var phone_no=/^(?:\+\d{2})?\d{10}(?:,(?:\+\d{2})?\d{10})*$/;

  
	var slider_title =$('#slider_title').val();
	var slider_desc =$('#slider_desc').val();
	var slider_image =$('#slider_image').val();
	var slider_position =$('#slider_position').val();

	

   if(!alpha.test(slider_title))
  {
    $("#slider_title").css({"border": "1px solid red"});
 
   $('#slider_title').focus();
   setTimeout(function() {
       $('#slider_title').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }

   if(!alpha.test(slider_desc))
  {
    $("#slider_desc").css({"border": "1px solid red"});
 
   $('#slider_desc').focus();
   setTimeout(function() {
       $('#slider_desc').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }



    if(slider_position==0)
  {
    $("#slider_position").css({"border": "1px solid red"});
 
   $('#slider_position').focus();
   setTimeout(function() {
       $('#slider_position').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }


  else
  {
    $('#sliderform').submit();
  }
});

</script>