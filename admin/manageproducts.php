<?php include 'layouts/Header.php'; ?>


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">Victoria Baker</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<?php include 'layouts/sidebar.php'; ?>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Manage</span> - product</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="datatable_styling.html">Manage</a></li>
							<li class="active">product</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Basic datatable -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Manage products</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							
						</div>

						<table class="table datatable-basic">
							<thead>
								<tr>
									<th>S.No</th>
									<th>Product Title</th>
									<th>Product Photo</th>
									<th>Medium</th>
									<th>Product Created Date</th>
									<th>Product Category</th>
									<th>Product Sub-category</th>
									
									<th>Product Status</th>
									<th class="text-center">Actions</th>
								</tr>
							</thead>
							<tbody>
							<?php 
							 $i=1;
							 $stmtProduct = $conn->prepare("SELECT * FROM tbl_product");
							 $stmtProduct->execute();
							 foreach ($stmtProduct->fetchALL() as $key => $product)
							 {
							 ?>
								<tr>
									<td><?php echo $i; $i++; ?></td>
									<td><?php echo $product['product_title']; ?></td>
									<td><img src="<?php echo $product['product_image']; ?>" height="100px" width="200px" /></td>
									<td><?php echo $product['product_created']; ?></td>

									   <td><?php
									    $stmtCat = $conn->prepare("SELECT product_medium FROM tbl_product WHERE product_id=:product_id");
									    $stmtCat->bindParam(':product_id', $product['product_id']);
									    $stmtCat->execute();
									    $category = $stmtCat->fetch();
									    echo $category['product_medium'];
									     ?></td>
									
									<td><?php
									    $stmtCat = $conn->prepare("SELECT category_title FROM tbl_category WHERE category_id=:category_id");
									    $stmtCat->bindParam(':category_id', $product['category_id']);
									    $stmtCat->execute();
									    $category = $stmtCat->fetch();
									    echo $category['category_title'];
									     ?></td>
									
									
										
									<td><?php 
									    $stmtCat = $conn->prepare("SELECT subcategory_title FROM tbl_subcategory WHERE sub_cat_id=:sub_cat_id");
									    $stmtCat->bindParam(':sub_cat_id', $product['sub_cat_id']);
									    $stmtCat->execute();
									    $subcategory = $stmtCat->fetch();
									    echo $subcategory['subcategory_title'];
									     ?></td>

									  

									<td>
									<?php if($product['product_status']==1): ?>
									<span class="label label-success">Active</span>
									<?php else: ?>
									<span class="label label-danger">Inactive</span>
									<?php endif; ?>
									</td>
									<td class="text-center">
										<ul class="icons-list">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-menu9"></i>
												</a>

												<ul class="dropdown-menu dropdown-menu-right">
													<li><a href="editproduct.php?id=<?php echo $product['product_id']; ?>"><i class=" icon-pencil7"></i>Edit product</a></li>
													<li><a href="deleteproduct.php?id=<?php echo $product['product_id']; ?>"><i class=" icon-trash"></i>Delete product</a></li>
													
												</ul>
											</li>
										</ul>
									</td>
								</tr>
							<?php } ?>
								
							</tbody>
						</table>
					</div>
					<!-- /basic datatable -->


					

				<?php include 'layouts/footer.php'; ?>