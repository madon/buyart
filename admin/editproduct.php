 <?php include 'layouts/Header.php';
$msg = '';
$product_id = $_GET['id'];
if(isset($_POST['savebtn']))
{

$product_title = $_POST['product_title'];
$product_desc = $_POST['product_desc'];
$product_status = $_POST['product_status'];

 $product_category = $_POST['product_category'];
 $product_subcategory = $_POST['product_subcategory'];

/*Upload Photo*/
	if(!empty($_FILES['product_image']['name']))
	{		
	$name=$_FILES['product_image']['name'];
	$src=$_FILES['product_image']['tmp_name'];
	$newname=time();
	$temp=explode(".",$name);
	$newpath=$newname.".".$temp[1];
	$name=$newpath;
	$target_dir="product_image/";
	$folder="product_image";
	if(!is_dir($folder))
	{
		mkdir($folder,0755);
	}
	move_uploaded_file($src,$target_dir.$name);
	$path = $target_dir.$name;

$stmtproduct = $conn->prepare("UPDATE  tbl_product  SET 
	 category_id=:category_id, sub_cat_id,=:sub_cat_id, product_title=:product_title, product_desc=:product_desc, product_size=:product_size, product_medium=:product_medium, product_status=:product_status, product_update=CURRENT_TIMESTAMP, product_image=:product_image WHERE product_id=:product_id");
$stmtProduct->bindParam('product_title',$product_title);
$stmtProduct->bindParam('product_desc',$product_desc);
$stmtProduct->bindParam('product_size',$product_size);
$stmtProduct->bindParam('product_medium',$product_medium);
$stmtProduct->bindParam('category_id',$category_id);
$stmtProduct->bindParam('sub_cat_id',$sub_cat_id);
$stmtProduct->bindParam('product_status',$product_status);
$stmtProduct->bindParam('product_image',$path);


	if($stmtproduct->execute())
	{
	$msg = '<div class="alert alert-info no-border">
			<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
				<span class="text-semibold">Congrats!</span>
				product Updated Successfully !!!</div>';
	}
	}

	else
	{

	$stmtproduct = $conn->prepare("UPDATE  tbl_product  SET 
	 category_id=:category_id, sub_cat_id,=:sub_cat_id, product_title=:product_title, product_desc=:product_desc, product_size=:product_size,
	product_medium=:product_medium, product_status=:product_status, product_update=CURRENT_TIMESTAMP WHERE product_id=:product_id");
$stmtProduct->bindParam('product_title',$product_title);
$stmtProduct->bindParam('product_desc',$product_desc);
$stmtProduct->bindParam('product_size',$product_size);
$stmtProduct->bindParam('product_medium',$product_medium);
$stmtProduct->bindParam('category_id',$category_id);
$stmtProduct->bindParam('sub_cat_id',$sub_cat_id);
$stmtProduct->bindParam('product_status',$product_status);
$stmtSlider->bindParam('product_id',$product_id);
	if($stmtproduct->execute())
	{
	$msg = '<div class="alert alert-info no-border">
			<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
				<span class="text-semibold">Congrats!</span>
				product Updated Successfully !!!</div>';
	}

	}
	



}


$stmtproductSelect = $conn->prepare("SELECT * FROM tbl_product WHERE product_id=:product_id");
$stmtproductSelect->bindParam(':product_id',$product_id);
$stmtproductSelect->execute();
$info = $stmtproductSelect->fetch();

 ?>

	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">Victoria Baker</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<?php  include 'layouts/sidebar.php'; ?>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Forms</span> - Product Add Form</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					
					<?php echo $msg; ?>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Form horizontal -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Add product</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							

						<form class="form-horizontal" action="" method="POST" name="productform" id="productform" enctype="multipart/form-data">
								<fieldset class="content-group">
									<legend class="text-bold">Information</legend>

									<div class="form-group">
										<label class="control-label col-lg-2">Product Title</label>
										<div class="col-lg-10">
											<input type="text" name="product_title" id="product_title" class="form-control" placeholder="product Title" value="<?php echo $info['product_title']; ?>">
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Product Description</label>
										<div class="col-lg-10">
											<textarea class="form-control" name="product_desc" id="product_desc" placeholder="Description..."><?php echo $info['product_desc']; ?></textarea>
										</div>
									 </div>

									 <div class="form-group">
										<label class="control-label col-lg-2">Product Size</label>
										<div class="col-lg-10">
											<input type="text" name="product_size" id="product_size" class="form-control" placeholder="product Title" value="<?php echo $info['product_size']; ?>">
										</div>
									 </div>

									 <div class="form-group">
										<label class="control-label col-lg-2">Medium </label>
										<div class="col-lg-10">
											<input type="text" name="product_medium" id="product_medium" class="form-control" placeholder="Product Medium" value="<?php echo $info['product_medium']; ?>">
										</div>
									 </div>


									 <div class="form-group">
										<label class="control-label col-lg-2">Previous Image</label>
										<div class="col-lg-10">
										<img src="<?php echo $info['product_image']; ?>" height="250px" width="500px">
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Product Image</label>
										<div class="col-lg-10">
											<input type="file" name="product_image" id="product_image" class="form-control" >
										</div>
									</div>
									

									<div class="form-group">
										<label class="control-label col-lg-2">Status</label>
										<div class="col-lg-10">
										 <input type="radio" name="product_status" <?php if($info['product_status']==1) echo 'checked'; ?> value="1"> Active

										 <input type="radio" name="product_status" <?php if($info['product_status']==0) echo 'checked'; ?> value="0"> Inactive
										</div>
									</div>

									

			                        <div class="form-group">
			                        	<label class="control-label col-lg-2">Product Category</label>
			                        	<div class="col-lg-10">
				                            <select  name="product_category" class="form-control" id="product_category">
				                            	<option value="0">--Select One--</option>
				                                 <?php $stmtCategory = $conn->prepare("SELECT * FROM tbl_category WHERE category_status='1'");
				                                     $stmtCategory->execute();
				                                     foreach ($stmtCategory as $key => $category):
				                                     ?>
				                                     <option value="<?php echo $category['category_id']; ?>"><?php echo $category['category_title']; ?></option>
				                                     <?php endforeach; ?>
				                               
				                            </select>
			                            </div>
			                        </div>
			                               <div class="form-group">
			                        	<label class="control-label col-lg-2">Product Sub-category</label>
			                        	<div class="col-lg-10">
				                            <select  name="product_subcategory" class="form-control" id="product_subcategory">
				                            	<option value="0">--Select One--</option>
				                                 <?php $stmtsubCategory = $conn->prepare("SELECT * FROM tbl_subcategory WHERE subcategory_status='1'");
				                                     $stmtsubCategory->execute();
				                                     foreach ($stmtsubCategory as $key => $subcategory):
				                                     ?>
				                                     <option value="<?php echo $subcategory['sub_cat_id']; ?>"><?php echo $subcategory['subcategory_title']; ?></option>
				                                     <?php endforeach; ?>
				                               
				                            </select>
			                            </div>
			                        </div>

									
								</fieldset>

								

								<div class="text-right">
									<button type="submit" name="savebtn" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</form>
						</div>
					</div>
					<!-- /form horizontal -->

					
					<?php  include 'layouts/footer.php'; ?>
<script type="text/javascript">
$('#productform').submit(function() {
  var filter = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  var number= /[0-9 -()+]+$/;
  var alpha= /^[a-zA-Z0-9!-”$%&’()*\+,\/;\[\\\]\/\s^_.`{|}~]+$/;
  var phone_no=/^(?:\+\d{2})?\d{10}(?:,(?:\+\d{2})?\d{10})*$/;

  
	var product_title =$('#product_title').val();
	var product_desc =$('#product_desc').val();
	//var product_image =$('#product_image').val();
	var product_category =$('#product_category').val();
	var product_subcategory =$('#product_subcategory').val();

	

   if(!alpha.test(product_title))
  {
    $("#product_title").css({"border": "1px solid red"});
 
   $('#product_title').focus();
   setTimeout(function() {
       $('#product_title').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }

     if(!alpha.test(product_size))
  {
    $("#product_size").css({"border": "1px solid red"});
 
   $('#product_size').focus();
   setTimeout(function() {
       $('#product_size').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }

   if(!alpha.test(product_desc))
  {
    $("#product_desc").css({"border": "1px solid red"});
 
   $('#product_desc').focus();
   setTimeout(function() {
       $('#product_desc').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }

     /*if(product_image=='')
  {
    $("#product_image").css({"border": "1px solid red"});
 
   $('#product_image').focus();
   setTimeout(function() {
       $('#product_image').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }*/

    if(product_category==0)
  {
    $("#product_category").css({"border": "1px solid red"});
 
   $('#product_category').focus();
   setTimeout(function() {
       $('#product_category').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }


  else
  {
    $('#productform').submit();
  }
});

</script>