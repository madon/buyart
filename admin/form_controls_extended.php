<?php include 'layouts/header.php'; ?>


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<?php include 'layouts/sidebar.php'; ?>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Forms</span> - Extended Controls</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="form_controls_extended.html">Forms</a></li>
							<li class="active">Extended controls</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Typeahead -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Twitter typeahead</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="content-group-lg">
										<h6 class="text-semibold">Basic usage</h6>
										<p class="content-group-sm">When initializing a typeahead, you pass the plugin method one or more datasets. The source of a dataset is responsible for computing a set of suggestions for a given query.</p>
										<input type="text" class="form-control typeahead-basic" placeholder="States of USA">
									</div>

									<div class="content-group-lg">
										<h6 class="text-semibold">Bloodhound engine</h6>
										<p class="content-group-sm">For more advanced use cases, rather than implementing the source for your dataset yourself, you can take advantage of <code>Bloodhound</code>, the <code>typeahead.js</code> suggestion engine.</p>
										<input type="text" class="form-control typeahead-bloodhound" placeholder="States of USA">
									</div>

									<div class="content-group-lg">
										<h6 class="text-semibold">Prefetched data</h6>
										<p class="content-group-sm">Prefetched data is fetched and processed on initialization. If the browser supports local storage, the processed data will be cached there to prevent additional network requests on subsequent page loads.</p>
										<input type="text" class="form-control typeahead-prefetched" placeholder="Countries">
									</div>

									<div class="content-group">
										<h6 class="text-semibold">Remote data</h6>
										<p class="content-group-sm">Remote data is only used when the data provided by local and prefetch is insufficient. In order to prevent an obscene number of requests being made to the remote endpoint, requests are rate-limited.</p>
										<input type="text" class="form-control typeahead-remote" placeholder="Oscar winners for Best Picture">
									</div>
								</div>

								<div class="col-md-6">
									<div class="content-group-lg">
										<h6 class="text-semibold">Custom templates</h6>
										<p class="content-group-sm">Custom templates give you full control over how suggestions get rendered making it easy to customize the look and feel of your typeahead. Requires <code>Handlebars.js</code> extension for compilation.</p>
										<input type="text" class="form-control typeahead-custom-templates" placeholder="Oscar winners for Best Picture">
									</div>

									<div class="content-group-lg">
										<h6 class="text-semibold">Multiple datasets</h6>
										<p class="content-group-sm">Multiple datasets give you visually separated sets of data inside Dropdown menu with custom titles, managed in <code>templates</code> option. This looks like a <code>&lt;optgroup></code> titles in selects.</p>
										<input type="text" class="form-control typeahead-multiple-datasets" placeholder="NBA and NHL teams">
									</div>

									<div class="content-group-lg">
										<h6 class="text-semibold">Dropdown height</h6>
										<p class="content-group-sm">To change the height of your dropdown menu, just wrap your input in some div with custom css styles and change necessary css properties or change it in css directly.</p>
										<div class="typeahead-scrollable">
											<input type="text" class="form-control typeahead-scrollable-menu" placeholder="Countries">
										</div>
									</div>

									<div class="content-group">
										<h6 class="text-semibold">RTL support</h6>
										<p class="content-group-sm">Twitter Typeahead supports <code>RTL</code> direction. Wrap your input in any div with <code>text-align: right;</code> property and add <code>dir="rtl"</code> to your input, now your dropdown menu is right aligned.</p>
										<input type="text" class="form-control typeahead-rtl-support" dir="rtl" placeholder="نعم">
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /typeahead -->


					<!-- Elastic textarea -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Elastic textarea</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<div class="row">
								<div class="col-md-4">
									<div class="content-group">
										<h6 class="text-semibold">Basic example</h6>
										<p class="content-group-sm">Drop Autosize into any existing website and it should Just Work™. The source is short and well commented if you are curious to how it works.</p>

										<div class="form-group">
											<textarea rows="4" cols="4" class="form-control elastic" placeholder="Textarea"></textarea>
										</div>

										<button type="button" class="btn btn-primary">Submit form</button>
									</div>
								</div>

								<div class="col-md-4">
									<div class="content-group">
										<h6 class="text-semibold">Manual triggering</h6>
										<p class="content-group-sm">When the value of a textarea has been changed through JavaScript, trigger the <code>autosize.resize</code> event immediately after to update the height.</p>

										<div class="form-group">
											<textarea rows="4" cols="4" class="form-control elastic elastic-manual" placeholder="Press 'Trigger Manually' button"></textarea>
										</div>

										<button type="button" class="btn btn-primary elastic-manual-trigger">Trigger manually</button>
									</div>
								</div>

								<div class="col-md-4">
									<div class="content-group">
										<h6 class="text-semibold">Removing autosize</h6>
										<p class="content-group-sm">Trigger the <code>autosize.destroy</code> event to remove autosize from a textarea element. Once <code>destroy</code> button clicked, autosize will be removed.</p>

										<div class="form-group">
											<textarea rows="4" cols="4" class="form-control elastic elastic-destroy" placeholder="Press 'Destroy' button to remove autosize"></textarea>
										</div>

										<button type="button" class="btn btn-primary elastic-destroy-trigger">Destroy Autosize</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /elastic textarea -->


		        	<!-- Masked inputs -->
		            <div class="panel panel-flat">
						<div class="panel-heading">
			                <h5 class="panel-title">Masked inputs</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

		                <div class="panel-body">
		                	<div class="row">
		                		<div class="col-md-4">
									<div class="form-group">
										<label>Date: </label>
			                        	<input type="text" class="form-control" data-mask="99/99/9999" placeholder="Enter starting date">
			                        	<span class="help-block">99/99/9999</span>
									</div>

									<div class="form-group">
										<label>Phone: </label>
			                        	<input type="text" class="form-control" data-mask="(999) 999-9999" placeholder="Enter your phone #">
			                        	<span class="help-block">(999) 999-9999</span>
									</div>

									<div class="form-group">
										<label>Phone + Ext: </label>
			                        	<input type="text" class="form-control" data-mask="(999) 999-9999 / x99999" placeholder="Enter your phone #">
			                        	<span class="help-block">(999) 999-9999 / x99999</span>
									</div>

									<div class="form-group">
										<label>Currency: </label>
			                        	<input type="text" class="form-control" data-mask="$999" placeholder="Enter amount in USD">
			                        	<span class="help-block">$999</span>
									</div>
		                		</div>

		                		<div class="col-md-4">
									<div class="form-group">
										<label>International format: </label>
			                        	<input type="text" class="form-control" data-mask="+39 999 999 999" placeholder="Enter your phone in international format">
			                        	<span class="help-block">+39 999 999 999</span>
									</div>
				                    
									<div class="form-group">
										<label>Tax ID: </label>
			                        	<input type="text" class="form-control" data-mask="99-9999999" placeholder="Enter your tax id">
			                        	<span class="help-block">99-9999999</span>
									</div>
				                    
									<div class="form-group">
										<label>SSN: </label>
			                        	<input type="text" class="form-control" data-mask="999-99-9999" placeholder="Enter your social security number">
			                        	<span class="help-block">999-99-9999</span>
									</div>

									<div class="form-group">
										<label>Credit card: </label>
			                        	<input type="text" class="form-control" data-mask="9999-9999-9999-9999" placeholder="Enter your credit card number">
			                        	<span class="help-block">9999-9999-9999-9999</span>
									</div>
		                		</div>

		                		<div class="col-md-4">
									<div class="form-group">
										<label>Product key: </label>
			                        	<input type="text" class="form-control" data-mask="a*-999-a999" placeholder="Enter your product key">
			                        	<span class="help-block">a*-999-a999</span>
									</div>
				                    
									<div class="form-group">
										<label>Purchase order: </label>
			                        	<input type="text" class="form-control" data-mask="aaa-999-***" placeholder="Enter your order #">
			                        	<span class="help-block">aaa-999-***</span>
									</div>
				                    
									<div class="form-group">
										<label>Percentage: </label>
			                        	<input type="text" class="form-control" data-mask="99%" placeholder="Enter value in %">
			                        	<span class="help-block">99%</span>
									</div>

									<div class="form-group">
										<label>ISBN: </label>
			                        	<input type="text" class="form-control" data-mask="999-99-999-9999-9" placeholder="Enter your ISBN">
			                        	<span class="help-block">999-99-999-9999-9</span>
									</div>
		                		</div>
		                	</div>	
		                </div>
		            </div>
		            <!-- /masked inputs -->


		        	<!-- Input formatter -->
		            <div class="panel panel-flat">
						<div class="panel-heading">
			                <h5 class="panel-title">Input formatter</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

		                <div class="panel-body">
		                	<div class="row">
		                		<div class="col-md-4">
									<div class="form-group">
										<label>Date: </label>
			                        	<input type="text" class="form-control" name="format-date" placeholder="Enter starting date">
			                        	<span class="help-block">99/99/9999</span>
									</div>

									<div class="form-group">
										<label>Phone number: </label>
			                        	<input type="text" class="form-control format-phone-number" placeholder="Enter your phone number">
			                        	<span class="help-block">(999) 999 - 9999</span>
									</div>

									<div class="form-group">
										<label>Phone + Ext: </label>
			                        	<input type="text" class="form-control" name="format-phone-ext" placeholder="Enter your phone number">
			                        	<span class="help-block">(999) 999-9999 / a999</span>
									</div>

									<div class="form-group">
										<label>Currency: </label>
			                        	<input type="text" class="form-control" name="format-currency" placeholder="Enter amount in USD">
			                        	<span class="help-block">$999.99</span>
									</div>
		                		</div>

		                		<div class="col-md-4">
									<div class="form-group">
										<label>International format: </label>
			                        	<input type="text" class="form-control" name="format-international-phone" placeholder="Enter your phone in international format">
			                        	<span class="help-block">+39 999 999 999</span>
									</div>
				                    
									<div class="form-group">
										<label>Tax ID: </label>
			                        	<input type="text" class="form-control" name="format-tax-id" placeholder="Enter your tax id">
			                        	<span class="help-block">99 - 9999999</span>
									</div>
				                    
									<div class="form-group">
										<label>SSN: </label>
			                        	<input type="text" class="form-control" name="format-ssn" placeholder="Enter your social security number">
			                        	<span class="help-block">999 - 99 - 9999</span>
									</div>

									<div class="form-group">
										<label>Credit card: </label>
			                        	<input type="text" class="form-control" name="format-credit-card" placeholder="Enter your credit card number">
			                        	<span class="help-block">9999 - 9999 - 9999 - 9999</span>
									</div>
		                		</div>

		                		<div class="col-md-4">
									<div class="form-group">
										<label>Product key: </label>
			                        	<input type="text" class="form-control" name="format-product-key" placeholder="Enter your product key">
			                        	<span class="help-block">a* - 999 - a999</span>
									</div>
				                    
									<div class="form-group">
										<label>Order number: </label>
			                        	<input type="text" class="form-control" name="format-order-number" placeholder="Enter your order #">
			                        	<span class="help-block">aaa - 999 - ***</span>
									</div>

									<div class="form-group">
										<label>ISBN: </label>
			                        	<input type="text" class="form-control" name="format-isbn" placeholder="Enter your ISBN">
			                        	<span class="help-block">999 - 99 - 999 - 9999 - 9</span>
									</div>

									<div class="form-group">
										<label>Visible formatted characters: </label>
			                        	<input type="text" class="form-control" name="format-persistent" placeholder="Enter your phone #">
			                        	<span class="help-block">+3 (999) 999-99-99</span>
									</div>
		                		</div>
		                	</div>	
		                </div>
		            </div>
		            <!-- /input formatter -->


					<!-- Password generator -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Password generator</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<div class="row">
								<div class="col-md-4">
									<div class="content-group">
										<h6 class="text-semibold">Checker inside label</h6>
										<p class="content-group-sm">Password checker is attached to the label, which appears below the input field. Empty label is hidden, if you add any text inside label, it'll stay visible.</p>

										<div class="form-group">
											<div class="label-indicator">
												<input type="text" class="form-control" placeholder="Enter your password">
												<span class="label label-block password-indicator-label"></span>
											</div>
										</div>

										<button type="button" class="btn btn-info generate-label">Generate 12 characters password</button>
									</div>
								</div>

								<div class="col-md-4">
									<div class="content-group">
										<h6 class="text-semibold">Label inside input</h6>
										<p class="content-group-sm">Rules are the same as for password checker inside label, but this one has absolute position and placed inside <code>form-group</code>. Empty label is also hidden.</p>

										<div class="form-group">
											<div class="label-indicator-absolute">
												<input type="text" class="form-control" placeholder="Enter your password">
												<span class="label password-indicator-label-absolute"></span>
											</div>
										</div>

										<button type="button" class="btn btn-info generate-label-absolute">Generate 10 characters password</button>
									</div>
								</div>

								<div class="col-md-4">
									<div class="content-group">
										<h6 class="text-semibold">Label inside addon</h6>
										<p class="content-group-sm">In this example password checker appears as <code>input-group-addon</code>. Note: addon must have any text or icon since group addon has table layout.</p>

										<div class="form-group">
											<div class="input-group group-indicator">
												<input type="text" class="form-control" placeholder="Enter your password">
												<span class="input-group-addon password-indicator-group">No password</span>
											</div>
										</div>

										<button type="button" class="btn btn-info generate-group">Generate 8 characters password</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /password generator -->


					<!-- Bootstrap maxlength -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Bootstrap maxlength</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<div class="content-group-lg">
										<h6 class="text-semibold">Basic example</h6>
										<p class="content-group-sm">Basic example of maxlength plugin. The badge will show up right below the input by default when the remaining chars are 10 or less. This is a default value and position.</p>
										<input type="text" class="form-control maxlength" maxlength="20" placeholder="Type 10 characters...">
									</div>

									<div class="content-group-lg">
										<h6 class="text-semibold">Label position</h6>
										<p class="content-group-sm">The field counter can be positioned at the top, bottom, left or right via <code>placement</code> option. You can also place the maxlength indicator on the corners: bottom-right, top-right, top-left, bottom-left and centered-right.</p>
										<input type="text" class="form-control maxlength-label-position" maxlength="20" placeholder="Centered right position">
									</div>

									<div class="content-group">
										<h6 class="text-semibold">Full featured</h6>
										<p class="content-group-sm">This is a complete example where all the options configured for the bootstrap-maxlength counter are setted. <strong>Please note:</strong> if the <code>alwaysShow</code> option is enabled, the <code>threshold</code> option is ignored.</p>
										<input type="text" class="form-control maxlength-options" maxlength="20" placeholder="Always visible with custom text">
									</div>
								</div>

								<div class="col-md-6">
									<div class="content-group-lg">
										<h6 class="text-semibold">Change the threshold value</h6>
										<p class="content-group-sm">Do you want the badge to show up when there are 20 chars or less? Use the <code>threshold</code> option. In this example label appears when the remaining chars are <code>15</code> or less.</p>
										<input type="text" class="form-control maxlength-threshold" maxlength="20" placeholder="Type 5 characters...">
									</div>

									<div class="content-group-lg">
										<h6 class="text-semibold">Custom options</h6>
										<p class="content-group-sm">Example with custom options. All options can be mixed: here label will show up after entering 10 characters, label has different color and shows up after the limit is reached.</p>
										<input type="text" class="form-control maxlength-custom" maxlength="20" placeholder="With Primary and Danger labels">
									</div>

									<div class="content-group">
										<h6 class="text-semibold">Textarea example</h6>
										<p class="content-group-sm">Bootstrap maxlength supports textarea as well as inputs. Even on old IE. Maxlength, attached to the textarea supports all available options and settings by default.</p>
										<textarea rows="3" cols="3" maxlength="225" class="form-control maxlength-textarea" placeholder="This textarea has a limit of 225 chars."></textarea>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /bootstrap maxlength -->


					<!-- Form actions -->
					<h6 class="content-group text-semibold">
						Form actions
						<small class="display-block">Action button positioning</small>
					</h6>

					<div class="row">
						<div class="col-md-4">
				        	<form action="#">
					            <div class="panel panel-flat">
									<div class="panel-heading">
						                <h6 class="panel-title">Left actions</h6>
										<div class="heading-elements">
											<ul class="icons-list">
						                		<li><a data-action="collapse"></a></li>
						                		<li><a data-action="reload"></a></li>
						                		<li><a data-action="close"></a></li>
						                	</ul>
					                	</div>
									</div>

					                <div class="panel-body">
										<div class="form-group">
											<label>Your name:</label>
											<input type="text" class="form-control" placeholder="Eugene Kopyov">
										</div>

										<div class="form-group">
											<label>Your password:</label>
											<input type="password" class="form-control" placeholder="Your strong password">
										</div>

										<div class="form-group">
											<label>Your message:</label>
											<textarea rows="5" cols="5" class="form-control" placeholder="Enter your message here"></textarea>
										</div>

										<button type="submit" class="btn bg-teal-400">Submit form <i class="icon-arrow-right14 position-right"></i></button>
									</div>
				                </div>
				            </form>
						</div>

						<div class="col-md-4">
				        	<form action="#">
					            <div class="panel panel-flat">
									<div class="panel-heading">
						                <h6 class="panel-title">Centered actions</h6>
										<div class="heading-elements">
											<ul class="icons-list">
						                		<li><a data-action="collapse"></a></li>
						                		<li><a data-action="reload"></a></li>
						                		<li><a data-action="close"></a></li>
						                	</ul>
					                	</div>
									</div>

					                <div class="panel-body">
										<div class="form-group">
											<label>Your name:</label>
											<input type="text" class="form-control" placeholder="Eugene Kopyov">
										</div>

										<div class="form-group">
											<label>Your password:</label>
											<input type="password" class="form-control" placeholder="Your strong password">
										</div>

										<div class="form-group">
											<label>Your message:</label>
											<textarea rows="5" cols="5" class="form-control" placeholder="Enter your message here"></textarea>
										</div>

										<div class="text-center">
											<button type="submit" class="btn bg-teal-400">Submit form <i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</div>
				                </div>
				            </form>
						</div>

						<div class="col-md-4">
				        	<form action="#">
					            <div class="panel panel-flat">
									<div class="panel-heading">
						                <h6 class="panel-title">Right actions</h6>
										<div class="heading-elements">
											<ul class="icons-list">
						                		<li><a data-action="collapse"></a></li>
						                		<li><a data-action="reload"></a></li>
						                		<li><a data-action="close"></a></li>
						                	</ul>
					                	</div>
									</div>

					                <div class="panel-body">
										<div class="form-group">
											<label>Your name: </label>
											<input type="text" class="form-control" placeholder="Eugene Kopyov">
										</div>

										<div class="form-group">
											<label>Your password: </label>
											<input type="password" class="form-control" placeholder="Your strong password">
										</div>

										<div class="form-group">
											<label>Your message: </label>
											<textarea rows="5" cols="5" class="form-control" placeholder="Enter your message here"></textarea>
										</div>

										<div class="text-right">
											<button type="submit" class="btn bg-teal-400">Submit form <i class="icon-arrow-right14 position-right"></i></button>
										</div>
									</div>
				                </div>
				            </form>
						</div>
					</div>
					<!-- /form actions -->


					<!-- Footer -->
					<div class="footer text-muted">
				<?php include 'layouts/footer.php'; ?>
					</div>
					<!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /content wrapper -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
