<?php include 'layouts/Header.php';
$msg = '';
if(isset($_POST['savebtn']))
{

$category_id = $_POST['category'];
$subcategory_title = $_POST['subcategory_title'];
$subcategory_desc = $_POST['subcategory_desc'];
$subcategory_status = $_POST['subcategory_status'];

/*Upload Photo*/
$name=$_FILES['sub_cat_image']['name'];
	$src=$_FILES['sub_cat_image']['tmp_name'];
	$newname=time();
	$temp=explode(".",$name);
	$newpath=$newname.".".$temp[1];
	$name=$newpath;
	$target_dir="sub_cat_image/";
	$folder="sub_cat_image";
	if(!is_dir($folder))
	{
		mkdir($folder,0755);
	}
	move_uploaded_file($src,$target_dir.$name);
	$path = $target_dir.$name;



$stmtsubcategory = $conn->prepare("INSERT INTO tbl_subcategory
	(category_id, subcategory_title, subcategory_desc, sub_cat_image, subcategory_status, subcategory_created)
	VALUES(:category_id, :subcategory_title, :subcategory_desc, :sub_cat_image,  :subcategory_status, CURRENT_TIMESTAMP)");
$stmtsubcategory->bindParam('category_id',$category_id);
$stmtsubcategory->bindParam('subcategory_title',$subcategory_title);
$stmtsubcategory->bindParam('subcategory_desc',$subcategory_desc);
$stmtsubcategory->bindParam('sub_cat_image',$path);
$stmtsubcategory->bindParam('subcategory_status',$subcategory_status);

if($stmtsubcategory->execute())
{
	$msg = '<div class="alert alert-info no-border">
			<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
				<span class="text-semibold">Congrats!</span>
				subcategory Added Successfully !!!</div>';
}

}

 ?>

	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="subcategory-content">
							<div class="media">
								<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">Victoria Baker</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<?php  include 'layouts/sidebar.php'; ?>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Forms</span> - subcategory Add Form</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					
					<?php echo $msg; ?>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Form horizontal -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Add Sub-subcategory</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							

							<form class="form-horizontal" action="" method="POST" name="subcategoryform" id="subcategoryform" enctype="multipart/form-data">
								<fieldset class="content-group">
									<legend class="text-bold">Information</legend>

									<div class="form-group">
										<label class="control-label col-lg-2">Subcategory Title</label>
										<div class="col-lg-10">
											<input type="text" name="subcategory_title" id="subcategory_title" class="form-control" placeholder="Subcategory Title">
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Subcategory Description</label>
										<div class="col-lg-10">
											<textarea class="form-control" name="subcategory_desc" id="subcategory_desc" placeholder="Description..."></textarea>
										</div>
									 </div>

									 <div class="form-group">
										<label class="control-label col-lg-2">Subcategory Image</label>
										<div class="col-lg-10">
											<input type="file" name="sub_cat_image" id="sub_cat_image" class="form-control" >
										</div>
									</div>

									

									<div class="form-group">
										<label class="control-label col-lg-2">Status</label>
										<div class="col-lg-10">
										 <input type="radio" name="subcategory_status" checked value="1"> Active

										 <input type="radio" name="subcategory_status" value="0"> Inactive
										</div>
									</div>
									<div class="form-group">
			                        	<label class="control-label col-lg-2">Category</label>
			                        	<div class="col-lg-10">
				                            <select  name="category" class="form-control" id="category">
				                            	<option value="0">--Select One--</option>
				                                 <?php $stmtCategory = $conn->prepare("SELECT * FROM tbl_category WHERE category_status='1'");
				                                     $stmtCategory->execute();
				                                     foreach ($stmtCategory as $key => $category):
				                                     ?>
				                                     <option value="<?php echo $category['category_id']; ?>"><?php echo $category['category_title']; ?></option>
				                                     <?php endforeach; ?>
				                               
				                            </select>
			                            </div>
			                        </div>					

			                       
			                       

									
								</fieldset>

								

								<div class="text-right">
									<button type="submit" name="savebtn" class="btn btn-primary">Add <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</form>
						</div>
					</div>
					<!-- /form horizontal -->

					
					<?php  include 'layouts/footer.php'; ?>
<script type="text/javascript">
$('#subcategoryform').submit(function() {
  var filter = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  var number= /[0-9 -()+]+$/;
  var alpha= /^[a-zA-Z0-9!-”$%&’()*\+,\/;\[\\\]\/\s^_.`{|}~]+$/;
  var phone_no=/^(?:\+\d{2})?\d{10}(?:,(?:\+\d{2})?\d{10})*$/;

  
	var subcategory_title =$('#subcategory_title').val();
	var subcategory_desc =$('#subcategory_desc').val();
	var subcategory_status =$('#subcategory_status').val();
	var sub_cat_image =$('#slider_image').val();

	

   if(!alpha.test(subcategory_title))
  {
    $("#subcategory_title").css({"border": "1px solid red"});
 
   $('#subcategory_title').focus();
   setTimeout(function() {
       $('#subcategory_title').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }

   if(!alpha.test(subcategory_desc))
  {
    $("#subcategory_desc").css({"border": "1px solid red"});
 
   $('#subcategory_desc').focus();
   setTimeout(function() {
       $('#subcategory_desc').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }
 
     if(sub_cat_image=='')
  {
    $("#sub_cat_image").css({"border": "1px solid red"});
 
   $('#sub_cat_image').focus();
   setTimeout(function() {
       $('#sub_cat_image').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }

    if(subcategory_status==0)
  {
    $("#subcategory_status").css({"border": "1px solid red"});
 
   $('#subcategory_status').focus();
   setTimeout(function() {
       $('#subcategory_status').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }


  else
  {
    $('#subcategoryform').submit();
  }
});

</script>