 <?php include 'layouts/Header.php';
$sub_cat_id=$_GET['id'];
$msg = '';
if(isset($_POST['savebtn']))
{

$subcategory_title = $_POST['subcategory_title'];
$subcategory_desc = $_POST['subcategory_desc'];
$subcategory_status = $_POST['subcategory_status'];

/*Upload Photo*/

	
$stmtsubcategory = $conn->prepare("UPDATE tbl_subcategory SET
	(subcategory_title, subcategory_desc, subcategory_status, subcategory_created )
	VALUES(:subcategory_title, :subcategory_desc, :subcategory_status, CURRENT_TIMESTAMP) WHERE sub_cat_id=:sub_cat_id");
$stmtsubcategory->bindParam('subcategory_title',$subcategory_title);
$stmtsubcategory->bindParam('subcategory_desc',$subcategory_desc);
$stmtsubcategory->bindParam('subcategory_status',$subcategory_status);
$stmtsubcategory->bindParam('sub_cat_id',$sub_cat_id);

if($stmtsubcategory->execute())
{
	$msg = '<div class="alert alert-info no-border">
			<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
				<span class="text-semibold">Congrats!</span>
				category Updated Successfully !!!</div>';
}

}

	



$stmtcategorySelect = $conn->prepare("SELECT * FROM tbl_subcategory WHERE sub_cat_id=:sub_cat_id");
$stmtcategorySelect->bindParam('sub_cat_id',$sub_cat_id);
$stmtcategorySelect->execute();
$info = $stmtcategorySelect->fetch();

 ?>


	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold">Victoria Baker</span>
									<div class="text-size-mini text-muted">
										<i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										<li>
											<a href="#"><i class="icon-cog3"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<?php  include 'layouts/sidebar.php'; ?>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Forms</span> - category Add Form</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					
					<?php echo $msg; ?>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Form horizontal -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Add category</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							

						<form class="form-horizontal" action="" method="POST" name="subcategoryform" id="subcategoryform" enctype="multipart/form-data">
								<fieldset class="content-group">
									<legend class="text-bold">Information</legend>

									<div class="form-group">
										<label class="control-label col-lg-2">Subcategory Title</label>
										<div class="col-lg-10">
											<input type="text" name="subcategory_title" id="subcategory_title" class="form-control" value="<?php echo $info['subcategory_title']; ?>">
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-2">Subcategory Description</label>
										<div class="col-lg-10">
											<textarea class="form-control" name="subcategory_desc" id="subcategory_desc" placeholder="Description..."><?php echo $info['subcategory_desc']; ?></textarea>
										</div>
									 </div>

									

									<div class="form-group">
										<label class="control-label col-lg-2">Status</label>
										<div class="col-lg-10">
										 <input type="radio" name="subcategory_status" checked value="1"> Active

										 <input type="radio" name="subcategory_status" value="0"> Inactive
										</div>
									</div>
									<div class="form-group">
			                        	<label class="control-label col-lg-2">Category</label>
			                        	<div class="col-lg-10">
				                            <select  name="category" class="form-control" id="category">
				                            	<option value="0">--Select One--</option>
				                                 <?php $stmtCategory = $conn->prepare("SELECT * FROM tbl_category WHERE category_status='1'");
				                                     $stmtCategory->execute();
				                                     foreach ($stmtCategory as $key => $category):
				                                     ?>
				                                     <option value="<?php echo $category['category_id']; ?>"><?php echo $category['category_title']; ?></option>
				                                     <?php endforeach; ?>
				                               
				                            </select>
			                            </div>
			                        </div>					

									

			                       
			                       

									
								</fieldset>

								

								<div class="text-right">
									<button type="submit" name="savebtn" class="btn btn-primary">update <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</form>
						</div>
					</div>
					<!-- /form horizontal -->

					
					<?php  include 'layouts/footer.php'; ?>
<script type="text/javascript">
$('#categoryform').submit(function() {
  var filter = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  var number= /[0-9 -()+]+$/;
  var alpha= /^[a-zA-Z0-9!-”$%&’()*\+,\/;\[\\\]\/\s^_.`{|}~]+$/;
  var phone_no=/^(?:\+\d{2})?\d{10}(?:,(?:\+\d{2})?\d{10})*$/;

  
	var category_title =$('#category_title').val();
	var category_desc =$('#category_desc').val();
	var category_image =$('#category_image').val();
	var category_category =$('#category_category').val();
	var category_subcategory =$('#category_subcategory').val();

	

   if(!alpha.test(category_title))
  {
    $("#category_title").css({"border": "1px solid red"});
 
   $('#category_title').focus();
   setTimeout(function() {
       $('#category_title').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }

   if(!alpha.test(category_desc))
  {
    $("#category_desc").css({"border": "1px solid red"});
 
   $('#category_desc').focus();
   setTimeout(function() {
       $('#category_desc').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }

     if(category_image=='')
  {
    $("#category_image").css({"border": "1px solid red"});
 
   $('#category_image').focus();
   setTimeout(function() {
       $('#category_image').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }

    if(category_category==0)
  {
    $("#category_category").css({"border": "1px solid red"});
 
   $('#category_category').focus();
   setTimeout(function() {
       $('#category_category').css({"border": "1px solid #ddd"});
   }, 5000);

        return false;
  }


  else
  {
    $('#categoryform').submit();
  }
});

</script>