<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php include'layouts/header.php'; ?>

		
<!-- //navigation -->
<!-- breadcrumbs -->
	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Description</li>
			</ol>
		</div>
	</div>
<!-- //breadcrumbs -->
<?php #
$product_id=0;
if(isset($_GET['product_id']))
{
	$product_id=$_GET['product_id'];
}
$select="SELECT * FROM tbl_product where product_id=$product_id";
$prepare=$conn->prepare($select);
if($prepare->execute())
{
$data=$prepare->fetch();

$product_name=$data['product_title'];
$product_desc=$data['product_desc'];
$product_size=$data['product_size'];
$product_medium=$data['product_medium'];
$product_image=$data['product_image'];
$price=$data['Price'];

}
else{
	die("Server error");
}
?>
	<div class="products">
		<div class="container">
			<div class="agileinfo_single">
				
				<div class="col-md-4 ">
					<a href=" Admin/<?php echo $product_image; ?>" target="_blank"><img height="500px" width="400px" alt="demo" src="Admin/<?php echo $product_image; ?>"></a>
				</div>
				<div class="col-md-8 agileinfo_single_right">
				<h2><?php echo $product_name;?></h2>
					
					<div class="w3agile_description">
						<h4>Description :</h4>
						<p><?php echo $product_desc;?></p>
					</div>

					<div class="w3agile_description">
						<h4>Size :</h4>
						<p><?php echo $product_size;?></p>
					</div>

					<div class="w3agile_description">
						<h4>Medium :</h4>
						<p><?php echo $product_medium;?></p>
					</div>

					<div class="snipcart-item block">
						<div class="snipcart-thumb agileinfo_single_right_snipcart">
							<h4 class="m-sing">price: Rs <?php echo $price; ?> 
							<!--<span>$25.00</span>-->
							</h4>
						</div>

						<div class="snipcart-details agileinfo_single_right_details">
							<form action="#" method="post">
								<fieldset>
									
									<input type="submit" name="submit" value="Add to cart" class="button">
									

								</fieldset>
								</form>
								
								<form action="buynow.php" method="post">								
								<fieldset>
									
									<input type="submit" name="submit" value="Buy Now" class="button">
								</fieldset>

							</form>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	
<!-- new -->
	<div class="newproducts-w3agile">
		<div class="container">
			<h3>Other Paintings</h3>
				<div class="agile_top_brands_grids">

					 
					 <?php 
					  $stmtproduct = $conn->prepare("SELECT * FROM tbl_product WHERE product_status='1'");
					  $stmtproduct->execute();
					  foreach ($stmtproduct->fetchAll() as $key => $product):
					  $product_id=$product['product_id']
					  
					?>
				

					<div class="col-md-3 top_brand_left-1">
						<div class="hover14 column">
							<div class="agile_top_brand_left_grid">
								<div class="agile_top_brand_left_grid_pos">
									
								</div>
								<div class="agile_top_brand_left_grid1">
									<figure>
										<div class="snipcart-item block">
											
										<div class="snipcart-thumb">
												<a href="<?php echo 'description.php?product_id='.$product_id; ?>"><img height="180px" alt=" " src="Admin/<?php echo $product['product_image']; ?>"></a>		
												<p><b><?php echo $product['product_title']; ?></b></p>
											
													<h4>Rs. <?php echo $product['Price']; ?></span></h4>

											</div>

											<div class="snipcart-details top_brand_home_details">
												<form action="#" method="post">
													<fieldset>
														<input type="hidden" name="cmd" value="_cart">
														<input type="hidden" name="add" value="1">
														<input type="hidden" name="business" value=" ">
														<input type="hidden" name="item_name" value="Fortune Sunflower Oil">
														<input type="hidden" name="amount" value="35.99">
														<input type="hidden" name="discount_amount" value="1.00">
														<input type="hidden" name="currency_code" value="USD">
														<input type="hidden" name="return" value=" ">
														<input type="hidden" name="cancel_return" value=" ">
														<input type="submit" name="submit" value="Add to cart" class="button">

													</fieldset>
												</form>
											</div>
										</div>
									</figure>
								</div>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
					
					
						<div class="clearfix"> </div>
				</div>
				
		</div>
	</div>

<!-- //new -->
<!---728x90--->
<!-- //footer -->
<?php include'layouts/footer.php'; ?>