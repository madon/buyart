 <!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<?php include'layouts/header.php'; ?>
<!-- //navigation -->
	<!-- main-slider -->
	
		<ul id="demo1">
			<?php include 'admin/app/call.php';
					  $stmtSlider = $conn->prepare("SELECT * FROM tbl_slider WHERE slider_status='1' ORDER BY slider_position ASC");
					  $stmtSlider->execute();
					  foreach ($stmtSlider->fetchAll() as $key => $slider):
					  
					  
					?>
			<li>
				<img src="Admin/<?php echo $slider['slider_image']; ?>" alt="" />
				  <div class="slide-desc">
					<h3  style="color:#f96706"><?php echo $slider['slider_title']; ?></h3>
				</div>
			</li>
		<?php endforeach; ?>	
		</ul>
		
	<!-- //main-slider -->

    	


<!-- new -->
	<div class="newproducts-w3agile">
		<div class="container">
			<h3>Category</h3>
				<div class="agile_top_brands_grids">
					  <?php 
					  $stmtproduct = $conn->prepare("SELECT * FROM tbl_subcategory WHERE subcategory_status='1'");
					  $stmtproduct->execute();
					  foreach ($stmtproduct->fetchAll() as $key => $product):
					  $sub_cat_id=$product['sub_cat_id']
					  
					?>
				
					<div class="col-md-3 top_brand_left-1" style="margin-bottom: 50px">
						<div class="hover14 column">
							<div class="agile_top_brand_left_grid">
								<div class="agile_top_brand_left_grid_pos">
									
								</div>
								<div class="agile_top_brand_left_grid1">
									<figure>
										<div class="snipcart-item block">
											<div class="snipcart-thumb">
												<a href="<?php echo 'details.php?id='.$sub_cat_id; ?>"><img height="180px" alt=" " src="Admin/<?php echo $product['sub_cat_image']; ?>"></a>		
												<p><b><?php echo $product['subcategory_title']; ?></b></p>
											
													<!--<h4>Rs. <?php echo $product['Price']; ?></span></h4>-->
											</div>
											<div class="snipcart-details top_brand_home_details">
												<form action="#" method="post">
													<fieldset>
														<input type="hidden" name="cmd" value="_cart">
														<input type="hidden" name="add" value="1">
														<input type="hidden" name="business" value=" ">
														<input type="hidden" name="item_name" value="Fortune Sunflower Oil">
														<input type="hidden" name="amount" value="35.99">
														<input type="hidden" name="discount_amount" value="1.00">
														<input type="hidden" name="currency_code" value="USD">
														<input type="hidden" name="return" value=" ">
														<input type="hidden" name="cancel_return" value=" ">
														<!--
														<input type="submit" name="submit" value="Add to cart" class="button"> 
														-->
													</fieldset>
												</form>
											</div>
										</div>
									</figure>
								</div>
							</div>
						</div>
					</div>

					<?php endforeach; ?>
				
						<div class="clearfix"> </div>
				</div>
		</div>
	</div>
<!-- //new -->
<?php include'layouts/footer.php'?>